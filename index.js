const crypto = require('crypto');
const base64 = require('base-64');
const bitcore = require('bitcore-lib');
const Message = require('bitcore-message');
const ethUtils = require('ethereumjs-util');

let TIME;

module.exports.templateTags = [{
    name: 'pbkdf2sha1',
    displayName: 'B2E => PBKDF2 SHA1',
    description: 'Generate a PBKDF2 SHA1 hash',
    args: [{
        displayName: 'Salt',
        description: 'Base64 salt used to hash the password',
        type: 'string',
        defaultValue: 'd2f4e5i9n3l69q4a'
    }, {
        displayName: 'Iterations',
        description: 'The number of iterations',
        type: 'number',
        defaultValue: 1000
    }, {
        displayName: 'Key Length',
        description: 'The length of the key in bytes',
        type: 'number',
        defaultValue: 24
    }, {
        displayName: 'Password',
        description: 'The password to hash',
        type: 'string',
    }],
    async run (context, salt, iterations, keyLen, password) {
        const slt = Buffer.from(salt, 'base64');
        const hash = crypto.pbkdf2Sync(password, slt, iterations, keyLen, 'sha1');
        return ('sha1:' + iterations.toString() + ':' + slt.toString('base64') + ':' + hash.toString('base64'));
    }
}, {
    name: 'clientId',
    displayName: 'B2E => Client ID',
    description: 'Choose from a list of valid client ID',
    args: [{
        displayName: 'Client ID',
        description: 'The client ID',
        type: 'enum',
        defaultValue: 'urn:b2e:btvgam',
        options: [{
            displayName: 'Beyond The Void',
            value: 'urn:b2e:btvgam',
        }, {
            displayName: 'Beyond The Void Monitoring',
            value: 'urn:b2e:btvmon',
        }, {
            displayName: 'Nexarium Market',
            value: 'urn:b2e:market',
        }, {
            displayName: 'Astero Clicker',
            value: 'urn:b2e:aclick',
        }, {
            displayName: 'Nexarium Auth Server',
            value: 'urn:b2e:autsrv',
        }, {
            displayName: 'Beyond The Void Server',
            value: 'urn:b2e:btvsrv',
        }]
    }],
    async run (context, id) {
        return (id);
    }
}, {
    name: 'messageToSign',
    displayName: 'B2E => Message To Sign',
    description: 'Generate a valid message to sign to add a BTC or ETH address',
    args: [{
        displayName: 'Player Name',
        description: 'The player name',
        type: 'string',
    }],
    async run (context, pName) {
        if (typeof (TIME) === 'undefined' || Date.now() - TIME >= 500)
            TIME = Date.now();
        return (Math.floor(TIME / 1000) + ':' + pName);
    }
}, {
    name: 'login',
    displayName: 'B2E => Login',
    description: 'Choose login variable (email or player name)',
    args: [{
        displayName: 'Login',
        description: 'The value to pass to the login',
        type: 'string'
    }],
    async run (context, login) {
        return (login);
    }
},{
    name: 'decodeJwt',
    displayName: 'B2E => Decode JWT',
    description: 'Get a part of a JWT token',
    args: [{
        displayName: 'Token',
        type: 'string'
    }, {
        displayName: 'Part',
        type: 'enum',
        options: [
            {displayName: 'Header', value: 'header'},
            {displayName: 'Payload', value: 'payload'}
        ]
    }, {
        displayName: 'Value',
        type: 'string'
    }],
    async run (context, jwt, part, value) {
        function decode(jwtPart, value) {
            let base64DecodedPart;
            let jsonParsedPart;
            let claimResult;

            try {
                base64DecodedPart = base64.decode(jwtPart);
            } catch (error) {
                throw (new Error(`JWT cannot be decoded (Base64 error): ${error.message}`));
            }
            try {
                jsonParsedPart = JSON.parse(base64DecodedPart);
            } catch (error) {
                throw (new Error(`JWT cannot be decoded (JSON error): ${error.message}`));
            }
            result = jsonParsedPart[value];
            if(result === undefined && value.length > 0) {
                throw new Error(`Value not found in JWT: ${value}`)
            }
            return (result);
        };

        const jwtParts = jwt.split('.')

        if (part === 'header')
            return (decode(jwtParts[0], value));
        else if(part === 'payload')
            return (decode(jwtParts[1], value));
        else if(part === 'signature')
            throw (new Error(`Decoding JWT signature is not supported`));
        else
            throw (new Error(`Unknow JWT part: ${part}`));
    }
}, {
    name: 'ethSign',
    displayName: 'B2E => ETH Sign',
    description: 'Sign a message from an ETH address',
    args: [{
        displayName: 'Private Key',
        description: 'The private key to sign with',
        type: 'string',
        defaultValue: '0x7b37565ebba9341b99863ff4bf69a9a882dd62529dadfe5b7b43ddc25a2b80ff'
    }, {
        displayName: 'Message',
        description: 'The message to sign',
        type: 'string'
    }],
    async run (context, privKey, msg) {
        let privKeyBuff = ethUtils.toBuffer(privKey);
        if (!ethUtils.isValidPrivate(privKeyBuff))
            throw (new Error(`invalid private key: ${privKey}`));
        let prefix = ethUtils.toBuffer('\u0019Ethereum Signed Message:\n' + ethUtils.toBuffer(msg).length.toString());
        msg = ethUtils.sha3(Buffer.concat([prefix, ethUtils.toBuffer(msg)]));
        msg = ethUtils.hashPersonalMessage(ethUtils.toBuffer(msg));
        let signature = ethUtils.ecsign(msg, privKeyBuff);
        let r = ethUtils.bufferToHex(ethUtils.toBuffer(signature.r));
        let s = ethUtils.bufferToHex(ethUtils.toBuffer(signature.s)).substring(2);
        let v = ethUtils.bufferToHex(ethUtils.toBuffer(signature.v)).substring(2);
        return (r + s + v);
    }
}, {
    name: 'btcSign',
    displayName: 'B2E => BTC Sign',
    description: 'Sign a message from a BTC address',
    args: [{
        displayName: 'Private Key',
        description: 'The private key to sign with',
        type: 'string',
        defaultValue: '5Kb8kLf9zgWQnogidDA76MzPL6TsZZY36hWXMssSzNydYXYB9KF'
    }, {
        displayName: 'Message',
        description: 'The message to sign',
        type: 'string'
    }],
    async run (context, privKey, msg) {
        if (!bitcore.PrivateKey.isValid(privKey))
            throw (new Error(`invalid private key: ${privKey}`));
        return (Message(msg).sign(bitcore.PrivateKey.fromWIF(privKey)));
    }
}, {
    name: 'getEthRandomPrivKey',
    displayName: 'B2E => Get ETH Random Private Key',
    description: 'Generate a random ethereum private key',
    args: [],
    async run (context) {
        return ('0x' + crypto.randomBytes(32).toString('hex'));
    }
}, {
    name: 'getBtcRandomPrivKey',
    displayName: 'B2E => Get BTC Random Private Key',
    description: 'Generate a random bitcoin private key',
    args: [],
    async run (context) {
        return (new bitcore.PrivateKey().toWIF());
    }
}, {
    name: 'getEthAddress',
    displayName: 'B2E => Get ETH Address',
    description: 'Generate a public ethereum address from a private key',
    args: [{
        displayName: 'Private Key',
        description: 'The private key of the wallet',
        type: 'string',
        defaultValue: '0x7b37565ebba9341b99863ff4bf69a9a882dd62529dadfe5b7b43ddc25a2b80ff'
    }],
    async run (context, privKey) {
        privKeyBuff = ethUtils.toBuffer(privKey);
        if (!ethUtils.isValidPrivate(privKeyBuff))
            throw (new Error(`invalid private key: ${privKey}`));
        return ('0x' + ethUtils.privateToAddress(privKeyBuff).toString('hex'));
    }
}, {
    name: 'getBtcAddress',
    displayName: 'B2E => Get BTC Address',
    description: 'Generate a public bitcoin address from a private key',
    args: [{
        displayName: 'Private Key',
        description: 'The private key of the wallet',
        type: 'string',
        defaultValue: '5Kb8kLf9zgWQnogidDA76MzPL6TsZZY36hWXMssSzNydYXYB9KF'
    }],
    async run (context, privKey) {
        if (!bitcore.PrivateKey.isValid(privKey))
            throw (new Error(`invalid private key: ${privKey}`));
        return (bitcore.PrivateKey.fromWIF(privKey).toPublicKey().toAddress());
    }
}];
